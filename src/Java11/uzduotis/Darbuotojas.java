package Java11.uzduotis;

public class Darbuotojas {
    private String vardas;
    private Double atlyginimas;
    private Adresas adresas;

    public Darbuotojas(String vardas, Double atlyginimas, Adresas adresas){
        this.vardas = vardas;
        this.atlyginimas = atlyginimas;
        this.adresas = adresas;
    }

    public String toString(){
        return "Vardas = " + getVardas() + " atlyginimas = " + getAtlyginimas() + " Adresas: " + getAdresas();
    }
    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public Double getAtlyginimas() {
        return atlyginimas;
    }

    public void setAtlyginimas(Double atlyginimas) {
        this.atlyginimas = atlyginimas;
    }

    public Adresas getAdresas() {
        return adresas;
    }

    public void setAdresas(Adresas adresas) {
        this.adresas = adresas;
    }

    public static class Adresas{
        private String miestas;
        private String gatve;
        private Integer numeris;

        // konstruktorius cia yra
        public Adresas(String miestas, String gatve, Integer numeris){
            this.miestas = miestas;
            this.gatve = gatve;
            this.numeris = numeris;

        }

        public String toString(){
            return "Miestas = " + getMiestas() + " Gatve = " + getGatve() + " Numeris = " + getNumeris();
        }

        public String getMiestas() {
            return miestas;
        }

        public void setMiestas(String miestas) {
            this.miestas = miestas;
        }

        public String getGatve() {
            return gatve;
        }

        public void setGatve(String gatve) {
            this.gatve = gatve;
        }

        public Integer getNumeris() {
            return numeris;
        }

        public void setNumeris(Integer numeris) {
            this.numeris = numeris;
        }
    }
}
