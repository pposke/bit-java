package Java11.uzduotis;

import java.util.Arrays;

public class UzduotisMain {
    public static void main(String[] args) {
        Darbuotojas darbuotojas1 = new Darbuotojas("Petras",
                500d, new Darbuotojas.Adresas("Kaunas",
                "Taikos pr.", 15));
        Darbuotojas darbuotojas2 = new Darbuotojas("Juozas",
                700d, new Darbuotojas.Adresas("Kaunas",
                "Savanoriu", 15));
        Darbuotojas darbuotojas3 = new Darbuotojas("Andrius",
                900d, new Darbuotojas.Adresas("Vilnius",
                "Savanoriu", 99));
        Darbuotojas darbuotojas4 = new Darbuotojas("Benas",
                1000d, new Darbuotojas.Adresas("Vilnius",
                "Savanoriu", 19));
        Darbuotojas darbuotojas5 = new Darbuotojas("Vytas",
                400d, new Darbuotojas.Adresas("Klaipeda",
                "Savanoriu", 15));

        Darbuotojas[] darbuotojai = new Darbuotojas[]{darbuotojas1,
                darbuotojas2, darbuotojas3, darbuotojas4, darbuotojas5};
        String[] testas = skirtingasMiestas(darbuotojai);

        System.out.println("Skirtingi miestai = " + Arrays.toString(testas));

    }
    public static String[] skirtingasMiestas(Darbuotojas[] darbuotojai) {
        String[] skirtingiMiestai = {darbuotojai[0].getAdresas().getMiestas()};
        for(int i = 1; i < darbuotojai.length; i++) {
            for (int j = 0; j < skirtingiMiestai.length; j++){
                if (!arEgzistuojaMasyve(skirtingiMiestai,darbuotojai[i].getAdresas().getMiestas())){
                    skirtingiMiestai = pridetiElementa(skirtingiMiestai,darbuotojai[i].getAdresas().getMiestas());

                    break;
                }
            }


        }
        return skirtingiMiestai;
    }

    public static Boolean arEgzistuojaMasyve(String[] darbuotojai, String miestas) {
        for (String el : darbuotojai) {
            if (el.equals(miestas)) {
                return true;
            }
        }
        return false;
    }

    public static String[] pridetiElementa(String[] masyvas,
                                           String elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }

}
