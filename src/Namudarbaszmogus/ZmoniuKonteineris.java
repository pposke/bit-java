package Namudarbaszmogus;

public class ZmoniuKonteineris {
    private Zmogus[] arr;

    public ZmoniuKonteineris(Zmogus[] arr) {
        this.arr = arr;
    }


    public Zmogus rastiVyriausia() {
        Integer max = 0;
        Integer maxIndeksas = 0;
        for(int i = 0; i < arr.length; i++) {
            if(max < arr[i].getAmzius()) {
                max = arr[i].getAmzius();
                maxIndeksas = i;
            }
        }
        return arr[maxIndeksas];
    }


}
