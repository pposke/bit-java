package Namudarbaszmogus;

public class Studentas extends Zmogus {
    private Integer kursas;
    private String studijos;


public Studentas (String vardas, String pavarde, Integer amzius, Integer kursas, String studijos){
    super(vardas, pavarde, amzius);
    this.kursas = kursas;
    this.studijos = studijos;
}

    @Override
    public String toString() {
        return "Vardas: " + getVardas() + " Pavarde: " + getPavarde() + " amzius: " + getAmzius() + " kursas: " + this.kursas + "studiju kryptis: " + this.studijos;
    }


}

