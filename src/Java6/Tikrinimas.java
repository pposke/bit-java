package Java6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class Tikrinimas {
    public static void main(String[] args) {
//     String failoKelias = new File("").getAbsolutePath()
//                + "/src/Java6/duomenys.txt";
//     skaitymas("", failoKelias);

//     Integer[] masyvas = {1,2,3,4,5};
//     Integer reiksme = grazintiReiksmia(masyvas,99);
//        System.out.println(reiksme);

        Integer paverstas = pakeistiTipa("5a5");
        System.out.println(paverstas);

        Character raide = gautiRaide("testas", 20);
        System.out.println(raide);

        Integer[] masyvasDu = {1,2,3,4,5};
        masyvasDu = pridetiPrieMasyvo(masyvasDu, 3);
        spausdinimas(masyvasDu);

    }

    // nd5



    public static Integer[] pridetiPrieMasyvo(Integer[] a,Integer e){
        try{
            a = Arrays.copyOf(a, a.length + 1);
            a[a.length + 1] = e;
            return a;

        }catch(NullPointerException ex){
            System.out.println(e);
            return null;

        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("Ivestas negzistuojantas masyvas");
            return null;
        }
    }
    public static void spausdinimas (Integer[] masyvas){
        try{
            for (int i = 0; i < masyvas.length; i++){
                System.out.println(masyvas[i]);
        }


        }catch(NullPointerException ex){
            System.out.println("ka tu cia rasineji lopas = ");

        }
    }






    public static Integer grazintiReiksmia( Integer[] masyvas, Integer indeksas){
        try{
            return masyvas[indeksas];
        }catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Ivestas neegzistuojantis indeksas");
            return masyvas[masyvas.length -1];
        }
    }

    public static Integer pakeistiTipa(String skaicius){
        try{
            return Integer.parseInt(skaicius);
        }catch(NumberFormatException ex){
            System.out.println("Nepavyko paversti i skaiciu");
            return null;
        }
    }

    public static Character gautiRaide(String zodis, Integer indeksas){
        try{
            return zodis.charAt(indeksas);
        }catch (Exception ex){
            System.out.println("Nepavyko rasti");
            return null;
        }

    }


    public static void skaitymas(String failoKelias, String visadaEgzistuojantisFailas){
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();

            while(eilute != null){
                System.out.println(eilute);
                eilute = skaitytuvas.readLine();
            }


        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
            System.out.println("Naudojamas kitas egzistuojantis failas");
            skaitymas(visadaEgzistuojantisFailas, visadaEgzistuojantisFailas);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
