package Figurosabstract;

public class Kvadratas extends Figuros {
    private Double krastine;
    private String pavadinimas;


    public Kvadratas(String pavadinimas, Double krastine) {

        this.krastine = krastine;
        this.pavadinimas = pavadinimas;
    }

    public String toString() {
        return "Figuros pavadinimas: " + gautiPavadinima() + " Plotis: " + gautiPlota() + " Perimetras: " + gautiPerimetra();
    }

    @Override
    public String gautiPavadinima() {

        return pavadinimas;
    }

    @Override
    public Double gautiPerimetra() {

        return krastine * 4;
    }

    @Override
    public Double gautiPlota() {

        return krastine * krastine;
    }
}
