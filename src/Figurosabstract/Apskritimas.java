package Figurosabstract;

public class Apskritimas extends Figuros {
    private Double krastine;
    private String pavadinimas;


    public Apskritimas(String pavadinimas, Double krastine) {
        this.krastine = krastine;
        this.pavadinimas = pavadinimas;
    }

    public String toString() {
        return "Figuros pavadinimas: " + gautiPavadinima() + " Plotis: " + gautiPlota() + " Perimetras: " + gautiPerimetra();
    }

    @Override
    public String gautiPavadinima() {
        return null;
    }

    @Override
    public Double gautiPerimetra() {

        return 2* Math.PI * krastine;
    }

    @Override
    public Double gautiPlota() {

        return Math.PI * (krastine*krastine);
    }
}
