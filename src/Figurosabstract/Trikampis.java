package Figurosabstract;

public class Trikampis extends Figuros {
    private Double plotis;
    private Double ilgis;
    private Double izambine;
    private String pavadinimas;


    public Trikampis(String pavadinimas, Double plotis, Double ilgis, Double izambine) {
        this.pavadinimas = pavadinimas;
        this.plotis = plotis;
        this.ilgis = ilgis;
        this.izambine = izambine;

    }

    public String toString() {
        return "Figuros pavadinimas: " + gautiPavadinima() + " Plotis: " + gautiPlota() + " Perimetras: " + gautiPerimetra();
    }

    @Override
    public String gautiPavadinima() {
        return null;
    }

    @Override
    public Double gautiPerimetra() {

        return plotis+ilgis;
    }

    @Override
    public Double gautiPlota() {

        return plotis * ilgis / 2;
    }
}
