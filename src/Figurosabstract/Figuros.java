package Figurosabstract;

public abstract class Figuros {
    public abstract String gautiPavadinima();
    public abstract Double gautiPlota();
    public abstract Double gautiPerimetra();

}
