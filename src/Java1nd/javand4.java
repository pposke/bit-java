package Java1nd;

import java.util.Scanner;

public class javand4 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite atstuma metrais");
        double s = reader.nextInt();
        System.out.println("Iveskite automibilio greiti");
        double v = reader.nextInt();
        double paversti = v*1000/3600;
        double t = s/paversti;
        String result = String.format("%.2f", t);

        System.out.println(" automobilio tuneli pravaziuos per " + result + " s ");
    }
}
