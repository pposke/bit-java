package Java1nd;

import java.util.Scanner;

public class javand1 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite pirmadienio pamokas");
        Integer pirm = reader.nextInt();
        System.out.println("Iveskite antradienio pamokas");
        Integer antr = reader.nextInt();
        System.out.println("Iveskite treciadienio pamokas");
        Integer trec = reader.nextInt();
        System.out.println("Iveskite ketvirtadienio pamokas");
        Integer ketv = reader.nextInt();
        System.out.println("Iveskite penktadienio pamokas");
        Integer penkt = reader.nextInt();

        Integer pamokos = pirm + antr + trec + ketv + penkt;

        System.out.println("Pamokos per savaite:" + pamokos);

        Integer valandos = ((pirm*45) + (antr*45) + (trec*45) + (ketv*45) + (penkt*45));

        System.out.println("Viso valandu:" + valandos );
    }
}
