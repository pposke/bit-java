package Java14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java14/Duomenys.txt";
        List<Car> masinos = new ArrayList<>();
        skaitymas(failoKelias, masinos);
        Collections.sort(masinos);
        System.out.println(masinos);
        System.out.println("Masinos tarp 2000 ir 2010 metu");
        List<Car> atrintos = rastiMasinasTarpMetu(masinos, 2000, 2010);
        System.out.println(atrintos);
        System.out.println("Seniausia masina yra");
        List<Car> seniausiaMasina = rastiSeniausiaMasina(masinos);
        System.out.println(seniausiaMasina);
        System.out.println("Naujausia masina");
        List<Car> naujausiaMasina = rastiNaujausia(masinos);
        System.out.println(naujausiaMasina);
        System.out.println("Naujesnes masinos nei 2010 ");
        List<Car> naujesnesMasinosNei = naujesneNei(masinos);
        System.out.println(naujesnesMasinosNei);
        System.out.println("Visos VW masinos");
        List<Car> volswagenMasinos = visosVwMasinos(masinos,"VW");
        System.out.println(volswagenMasinos);
        System.out.println("Didziausias variklo turis");
        List<Car> didziaussiasVariklioTuris = didziausiasVariklioTuris(masinos);
        System.out.println(didziaussiasVariklioTuris);
    }
    public static List<Car> rastiSeniausiaMasina(List<Car> masinos){
        List<Car> seniausiosMasinos = new ArrayList<>();
        Integer minMetai = 2020;
        Integer Indeksas = 0;
        for (int i = 0; i < masinos.size(); i++){
            if (masinos.get(i).getMetai() < minMetai){
//                seniausiosMasinos.add(masinos.get(i));
                minMetai = masinos.get(i).getMetai();
                Indeksas = i;

            }
        }
        seniausiosMasinos.add(masinos.get(Indeksas));
        return seniausiosMasinos;
    }

    public static List<Car> rastiNaujausia(List<Car> masinos){
        List<Car> naujausiaMasina = new ArrayList<>();
        Integer naujasiMetai = 1990;
        Integer Indeksas = 0;
        for (int i = 0; i < masinos.size(); i++){
            if (masinos.get(i).getMetai() > naujasiMetai){
//                seniausiosMasinos.add(masinos.get(i));
                naujasiMetai = masinos.get(i).getMetai();
                Indeksas = i;

            }
        }
        naujausiaMasina.add(masinos.get(Indeksas));
        return naujausiaMasina;
    }

    public static List<Car> naujesneNei(List<Car> masinos){
        List<Car> naujesnesNei = new ArrayList<>();
        Integer naujesnesMasinosNei = 2010;
        for (int i = 0; i < masinos.size(); i++){
            if (masinos.get(i).getMetai() >= naujesnesMasinosNei){



                naujesnesNei.add(masinos.get(i));
            }
        }

        return naujesnesNei;
    }

    public static List<Car> visosVwMasinos(List<Car> masinos,String marke){
        List<Car> vwMasinos = new ArrayList<>();

        for (int i = 0; i < masinos.size(); i++){
            if (masinos.get(i).getGamintojas().equals(marke)){
//                vwMasinos = masinos.get(i).getGamintojas();
                vwMasinos.add(masinos.get(i));
            }
        }

        return vwMasinos;
    }

    public static List<Car> didziausiasVariklioTuris(List<Car> masinos){
        List<Car> didziausiasVariklis = new ArrayList<>();
        Double turis = 3.0d;
        Integer Indeksas = 0;
        for (int i = 0; i < masinos.size(); i++){
            if (masinos.get(i).getVariklioTuris() >= turis ){
//                vwMasinos = masinos.get(i).getGamintojas();
                turis = masinos.get(i).getVariklioTuris();
            Indeksas = i;

            }
        }
        didziausiasVariklis.add(masinos.get(Indeksas));
        return didziausiasVariklis;
    }



    public static List<Car> rastiMasinasTarpMetu(List<Car> masinos,
                                                 Integer nuo,
                                                 Integer iki) {
        List<Car> atrinktos = new ArrayList<>();
        for(int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > nuo &&
                    masinos.get(i).getMetai() < iki) {
                atrinktos.add(masinos.get(i));
            }
        }
        return atrinktos;
    }

    // pasiziureti, kaip nuskaityti visus failus esancius duomenu txt faile, o ne tikrinti pagal nurodyta skaiciu

    public static void skaitymas(String failas,List<Car> masinos) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < kiekEiluciu; i++) {
                String[] eilutesDuomenys = eilute.split(" ");
                String marke = eilutesDuomenys[0];
                String modelis = eilutesDuomenys[1];
                Integer metai = Integer.parseInt(eilutesDuomenys[2]);
                Double kaina = Double.parseDouble(eilutesDuomenys[3]);
                Double turis = Double.parseDouble(eilutesDuomenys[4]);
                String kuroTipas = eilutesDuomenys[5];
                Car masina = new Car(marke, modelis, metai, kaina, turis, kuroTipas);
                masinos.add(masina);
                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }


}
