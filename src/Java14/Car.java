package Java14;

import Java12.NamuDarbas.Masina;

public class Car implements Comparable<Car>{
    private String gamintojas;
    private String modelis;
    private Integer metai;
    private Double kaina;
    private Double variklioTuris;
    private String kuroTipas;

    public Car(String gamintojas, String modelis, Integer metai, Double kaina, Double variklioTuris, String kuroTipas) {
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.metai = metai;
        this.kaina = kaina;
        this.variklioTuris = variklioTuris;
        this.kuroTipas = kuroTipas;
    }

    public int compareTo(Car obj) {
        return getMetai().compareTo(obj.getMetai());
    }
    @Override
    public String toString() {
        return "Car{" +
                "gamintojas='" + gamintojas + '\'' +
                ", modelis='" + modelis + '\'' +
                ", metai=" + metai +
                ", kaina=" + kaina +
                ", variklioTuris=" + variklioTuris +
                ", kuroTipas='" + kuroTipas + '\'' +
                '}' + "\n";
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public void setModelis(String modelis) {
        this.modelis = modelis;
    }

    public Integer getMetai() {
        return metai;
    }

    public void setMetai(Integer metai) {
        this.metai = metai;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }

    public Double getVariklioTuris() {
        return variklioTuris;
    }

    public void setVariklioTuris(Double variklioTuris) {
        this.variklioTuris = variklioTuris;
    }

    public String getKuroTipas() {
        return kuroTipas;
    }

    public void setKuroTipas(String kuroTipas) {
        this.kuroTipas = kuroTipas;
    }
}
