package Java14lambda;

import com.sun.source.tree.Tree;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ZmoniuMain {

    public static void main(String[] args) {
        Zmogus obj1 = new Zmogus("petras", "petraitis", "123456");
        Zmogus obj2 = new Zmogus("Dzionis", "Bravo", "896574");
        Zmogus obj3 = new Zmogus("Tadas", "Tomaitis", "787878");
        Zmogus obj4 = new Zmogus("Titas", "asdads", "654689");
        Zmogus obj5 = new Zmogus("Paulius", "ffg", "223213");
        Map<String, Zmogus> mapas = new TreeMap<>();
        mapas.put(obj1.getAsmensKodas(), obj1);
        mapas.put(obj2.getAsmensKodas(), obj2);
        mapas.put(obj3.getAsmensKodas(), obj3);
        mapas.put(obj4.getAsmensKodas(), obj4);
        mapas.put(obj5.getAsmensKodas(), obj5);
        System.out.println(mapas);
    }
}
