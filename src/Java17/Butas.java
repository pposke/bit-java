package Java17;

public class Butas {
    private Integer butoNr;
    private String butoAdresas;
    private Integer kambariuSk;
    private Double kvadratura;
    private Double kaina;

    @Override
    public String toString() {
        return "Butas" + " " +
                "butoNr=" + butoNr +
                ", butoAdresas='" + butoAdresas + '\'' +
                ", kambariuSk=" + kambariuSk +
                ", kvadratura=" + kvadratura +
                ", kaina=" + kaina +
                "\n";
    }

    public Butas(Integer butoNr, String butoAdresas, Integer kambariuSk, Double kvadratura, Double kaina) {
        this.butoNr = butoNr;
        this.butoAdresas = butoAdresas;
        this.kambariuSk = kambariuSk;
        this.kvadratura = kvadratura;
        this.kaina = kaina;
    }

    public Integer getButoNr() {
        return butoNr;
    }

    public void setButoNr(Integer butoNr) {
        this.butoNr = butoNr;
    }

    public String getButoAdresas() {
        return butoAdresas;
    }

    public void setButoAdresas(String butoAdresas) {
        this.butoAdresas = butoAdresas;
    }

    public Integer getKambariuSk() {
        return kambariuSk;
    }

    public void setKambariuSk(Integer kambariuSk) {
        this.kambariuSk = kambariuSk;
    }

    public Double getKvadratura() {
        return kvadratura;
    }

    public void setKvadratura(Double kvadratura) {
        this.kvadratura = kvadratura;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }
}

