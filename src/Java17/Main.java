package Java17;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String butuDuomenys = new File("").getAbsolutePath()
                + "/src/Java17/Duomenys.txt";

        String kriterjausFailas = new File("").getAbsolutePath()
                + "/src/Java17/Kriterijai.txt";

        String rezultatuFailas = new File("").getAbsolutePath()
                + "/src/Java17/Rezultatai.txt";

        Map<Integer, Butas> butai = new HashMap<>();
        skaitymasButu(butuDuomenys, butai);
//        System.out.println(butai);

        Kriterijus kriterijus = new Kriterijus();
        skaitomKriterijus(kriterjausFailas, kriterijus);
//        System.out.println(kriterijus.getKambariuSkNuo());
        List<Butas> atrinkti = atrinktiTinkamusButus(butai,kriterijus);
        System.out.println(atrinkti);
        rasyti(rezultatuFailas, atrinkti);

    }

    public static List<Butas> atrinktiTinkamusButus(Map<Integer, Butas> butai,Kriterijus kriterijus ) {
        List<Butas> atrinkti = new ArrayList<>();
        for (Butas butas: butai.values())
            if (butas.getKambariuSk() >= kriterijus.getKambariuSkNuo() && butas.getKambariuSk() <= kriterijus.getKambariuSkIki()
            && butas.getKvadratura() >= kriterijus.getKvadraturaNuo() && butas.getKvadratura() <= kriterijus.getKvadraturaIki() &&
            butas.getKaina() >= kriterijus.getKainaNuo() && butas.getKaina() <= kriterijus.getKainaIki())
                atrinkti.add(butas);

        return atrinkti;
    }

    public static void rasyti(String rezFailas, List<Butas> atrinkti){
        try(BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezFailas)) ) {
            spausdinimas.write("Atrinkti butai: \n");
            for (Butas butas: atrinkti){
                spausdinimas.write(butas.toString());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void skaitymasButu(String failas, Map<Integer, Butas> butai) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < kiekEiluciu; i++){
                String[] eilDuomenys = eilute.split(" ");
                Integer butoNr = Integer.parseInt(eilDuomenys[0]);
                String adresas = eilDuomenys[1] + " " + eilDuomenys[2] + " " + eilDuomenys[3];
                Integer kambariuKiekis = Integer.parseInt(eilDuomenys[4]);
                Double kvadratura = Double.parseDouble(eilDuomenys[5]);
                Double kaina = Double.parseDouble(eilDuomenys[6]);
                Butas butas = new Butas(butoNr,adresas,kambariuKiekis,kvadratura,kaina);
                butai.put(butas.getButoNr(),butas);
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("something else went wrong");
        }

    }


    public static void skaitomKriterijus(String failas, Kriterijus kriterijus) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {

            String eilute = skaitytuvas.readLine();
            String[] eilDuomenys = eilute.split(" ");
            Integer kambariuSkNuo = Integer.parseInt(eilDuomenys[0]);
            Integer kambariuSkIki = Integer.parseInt(eilDuomenys[1]);

            eilute = skaitytuvas.readLine();
            eilDuomenys = eilute.split(" ");
            Double kvadraturaNuo = Double.parseDouble(eilDuomenys[0]);
            Double kvadraturaIki = Double.parseDouble(eilDuomenys[1]);

            eilute = skaitytuvas.readLine();
            eilDuomenys = eilute.split(" ");
            Double kainaNuo = Double.parseDouble(eilDuomenys[0]);
            Double kainaIiki = Double.parseDouble(eilDuomenys[1]);

            kriterijus.setKambariuSkNuo(kambariuSkNuo);
            kriterijus.setKambariuSkIki(kambariuSkIki);
            kriterijus.setKvadraturaNuo(kvadraturaNuo);
            kriterijus.setKvadraturaIki(kvadraturaIki);
            kriterijus.setKainaNuo(kainaNuo);
            kriterijus.setKainaIki(kainaIiki);


        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("something else went wrong");
        }

    }
}

