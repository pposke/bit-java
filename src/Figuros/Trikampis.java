package Figuros;

public class Trikampis extends Figura {
    private Double plotis;
    private Double ilgis;
    private Double izambine;


    public Trikampis (String vardas, Double plotis, Double ilgis, Double izambine) {
        super(vardas);
        this.plotis = plotis;
        this.ilgis = ilgis;
        this.izambine = izambine;
    }

    public String toString() {
        return  "\n" + "Figuros pavadinimas: " + getVardas() + " Plotas: " + gautiPlota() + " Perimetras: " + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return plotis+ilgis;
    }

    public Double gautiPlota() {
        return plotis * ilgis / 2;
    }
}
