package Figuros;


public class Staciakampis extends Figura {
    private Double plotis;
    private Double ilgis;

    public Staciakampis(String vardas, Double plotis,
                        Double ilgis) {
        super(vardas);
        this.ilgis = ilgis;
        this.plotis = plotis;
    }

    public String toString() {
        return "\n" + "Figuros pavadinimas: " + getVardas() + " Plotis: " + gautiPlota() + " Perimetras: " + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return ilgis * 2 + plotis * 2;
    }

    public Double gautiPlota() {
        return ilgis * plotis;
    }
}


