package Figuros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class FigurosMain {
    public static void main(String[] args) {
//        Staciakampis naujasStaciakampis = new Staciakampis("s1",
//                5.0, 9.2);
//        System.out.println(naujasStaciakampis);
        String failoKelias = new File("").getAbsolutePath()
                + "/src/Figuros/Duomenys.txt";
        Figura[] figuros = skaityti(failoKelias);
        System.out.println(figuros);
        System.out.println(Arrays.toString(figuros));


        Figura didziausiasPLotas = didziausiasPlotas(figuros);
        System.out.println("didziausias plotas yra: " + didziausiasPLotas);


    }

    public static Figura didziausiasPlotas (Figura[] masyvas){
        Double max = 0d;
        Integer indeksas = 0;
        for (int i = 0; i < masyvas.length; i++){
            Double plotas = 0d;
            if (masyvas[i] instanceof Kvadratas){
                Kvadratas obj = (Kvadratas)masyvas[i];
                plotas = obj.gautiPlota();
            }else if (masyvas[i] instanceof Staciakampis){
                Staciakampis obj = (Staciakampis)masyvas[i];
                plotas = obj.gautiPlota();
            }else {
                Trikampis obj = (Trikampis)masyvas[i];
                plotas = obj.gautiPlota();
            }
            if (max < plotas){
                max = plotas;
                indeksas = i;
            }


        }
        return masyvas[indeksas];
    }


    public static Figura[] skaityti(String failoKelias) {
        Figura[] figuros = new Figura[3];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine(); // Nuskaito pirma eilute
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                if (eilDuomenys.length == 1) { // Kvadratas
                    Double krastine = Double.parseDouble(eilDuomenys[0]);
                    figuros[indeksas] = new Kvadratas("kvadratas", krastine);
                } else if (eilDuomenys.length == 2) { // Staciakampis
                    Double krastine1 = Double.parseDouble(eilDuomenys[0]);
                    Double krastine2 = Double.parseDouble(eilDuomenys[1]);
                    figuros[indeksas] = new Staciakampis("stac", krastine1, krastine2);
                } else { // Trikampis
                    Double krastinteTri1 = Double.parseDouble(eilDuomenys[0]);
                    Double krastineTri2 = Double.parseDouble(eilDuomenys[1]);
                    Double krastineTri3 = Double.parseDouble(eilDuomenys[2]);
                    figuros[indeksas] = new Trikampis("trikampis", krastinteTri1, krastineTri2, krastineTri3);

                }
                indeksas++;
                eilute = skaitytuvas.readLine();
            }



        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return figuros;
    }

//   public static Figura didziausiasPlotas (Figura[] masyvas){
//        Figura didziausias = masyvas[0];
//        for (int i; i < masyvas.length; i++){
//
//        }if (didziausias < masyvas[i]){
//            didziausias = masyvas[i];
//       }
//        return masyvas;
//   }
}
