package Figuros;

public class Kvadratas extends Figura {
    private Double plotis;


    public Kvadratas(String vardas, Double plotis) {
        super(vardas);
        this.plotis = plotis;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getVardas() + " Plotis: " + gautiPlota() + " Perimetras: " + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return plotis * 4;
    }

    public Double gautiPlota() {

        return plotis * plotis;
    }
}
