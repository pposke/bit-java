package Java5;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;


public class Java5metodai {
    public static void main(String[] args) {
        Integer[] array = {1,2,3,4,5,6};
        String[] zodziai = {"a", "testas", "vapse"};
        spausdinimas(array);
        spausdinimas(zodziai);
        // nd 3
        Double vidurkis = vidurkis(array);
        System.out.println("Vidurkis + " + vidurkis);

        // nd 4

        Integer didziausias = didziausias(array);
        System.out.println("didziausias yra = " + didziausias);

        // nd 5

        Integer maziausias = maziausias(array);
        System.out.println("maziausias skaicius yra = " + maziausias);


        // nd 6
//        Integer[] masyvas5 = {1,5,4,6,8,20,15,16,18};
//        Integer grazinti = grazinti(masyvas5);
//        System.out.println("skaicia nera didesni nei 10 = " + grazinti);


        /// nd 7
        int[] rikiavimoMasyvas = {9, 32, 22, 14, 5, 66};
        System.out.print("surikiuotas = : "); //1,55,66,77,88,99
        surikiuoti(rikiavimoMasyvas);
        atspausdintiDuomenys(rikiavimoMasyvas);

     /// nd 8
        String[] visiZodziai = {"Pamidorai" , "Agurkai", "Juodieji pipirai", "Pica", "Sautuvas"};
        String ilgiausiasZodis = gaukIlgiausiaZodi(visiZodziai);
        System.out.println("ilgiausias zodis yra  = " + ilgiausiasZodis);


    /// nd 9
        Scanner in = new Scanner(System.in);
        System.out.print("Ivesk zodi: ");
        String tikrinamZodi = in.nextLine();
        System.out.print("Vidurine zodzio raide/es yra = " + vidurineRaide(tikrinamZodi));

    }

    public static void spausdinimas (Integer[] masyvas){
        for (int i = 0; i < masyvas.length; i++){
            arLyginis(masyvas[i]);
            System.out.println(masyvas[i]);
        }
    }

    public static void spausdinimas (String[] masyvas){
        for (int i = 0; i < masyvas.length; i++){
            System.out.println(masyvas[i]);
        }

        Integer suma = suma(9, 9);
        System.out.println(suma);

        arLyginis(5);


    }


    public static Integer suma(Integer a, Integer b){
        return a + b;
    }


    public static void arLyginis(Integer skaicius){
        if (skaicius % 2 == 0){
//            System.out.println(skaicius + "skaicius yra lyginis");
        }else{
//            System.out.println(skaicius + "skaicius nera lyginis");
        }
    }

    public static Double vidurkis (Integer[] masyvas){
        Double suma = 0d;
        for (int i = 0; i < masyvas.length; i++){
            suma += masyvas[i];
        }
        return suma / masyvas.length;
    }

    public static Integer didziausias (Integer[] array){
        Integer didziausias = array[0];
        for (int i = 1; i < array.length; i++){
            if (array[i] > didziausias){
                didziausias = array[i];
            }
        }
        return didziausias;
    }

    public static Integer maziausias (Integer[] array){
        Integer maziausias = array[0];
        for (int i = 1; i < array.length; i++){
            if (array[i] < maziausias){
                maziausias = array[i];
            }
        }
        return maziausias;
    }


    // nd 5





    public static Integer[] pridetiElementus(Integer[] masyvas, Integer elementas){
     masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
     masyvas[masyvas.length - 1] = elementas;
     return masyvas;
     }


    private static void surikiuoti(int[] input) {
        surikiuoti(input, true);
    }

    private static void surikiuoti(int[] input, boolean ascending) {

        int inputLength = input.length;
        int temp;
        boolean is_sorted;

        for (int i = 0; i < inputLength; i++) {

            is_sorted = true;

            for (int j = 1; j < (inputLength - i); j++) {

                if (ascending) {
                    if (input[j - 1] > input[j]) {
                        temp = input[j - 1];
                        input[j - 1] = input[j];
                        input[j] = temp;
                        is_sorted = false;
                    }
                } else {
                    if (input[j - 1] < input[j]) {
                        temp = input[j - 1];
                        input[j - 1] = input[j];
                        input[j] = temp;
                        is_sorted = false;
                    }

                }

            }

            // is sorted? then break it, avoid useless loop.
            if (is_sorted) break;

        }

    }
    private static void atspausdintiDuomenys(int[] data) {
        String result = Arrays.stream(data)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(","));
        System.out.println(result);
    }




    public static String gaukIlgiausiaZodi(String[] visiZodziai){
        Integer maxDydis = 0;
        String ilgiausiasZodis = null;
        for (String z: visiZodziai)
            if (z.length() > maxDydis){
                maxDydis = z.length();
                ilgiausiasZodis = z;
            }
        return ilgiausiasZodis;
    }

    public static String vidurineRaide(String zodis){
     int position;
     int lenght;
     if (zodis.length() % 2 == 0){
         position = zodis.length() / 2 - 1;
         lenght = 2;
     } else{
         position = zodis.length() / 2;
         lenght = 1;


     }
     return zodis.substring(position,position + lenght);
    }





    }



