package JAVA3KL;

import java.io.*;

public class Java3ND8 {
    public static void main(String[] args) {
        String failoKelias = new File( "").getAbsolutePath()
                + "/src/JAVA3KL/Tekstas.txt";
        String zodziuMax = new File( "").getAbsolutePath()
                + "/src/JAVA3KL/zodziuMax.txt";

        String duomenuTekstas = "";
        Integer zozdiuKiekis = 0;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))){
            String eilute = skaitytuvas.readLine();



            while(eilute != null){
                duomenuTekstas += "\n" + eilute;
                System.out.println(eilute);
                String[] eilutesReiksmes = eilute.split( " ");
                eilute = skaitytuvas.readLine();
                zozdiuKiekis += eilutesReiksmes.length;
                System.out.println("zodziu kiekis = " + zozdiuKiekis);

            }

        }catch (FileNotFoundException ex){
            System.out.println("Failas nerastas");
        } catch (Exception e){
            System.out.println(e);
        }

        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(zodziuMax))) {
            spausdinimas.write("parasem i faila");
            spausdinimas.write(duomenuTekstas);
            spausdinimas.write( "\n" + "Zodziu kiekis yra = " + zozdiuKiekis);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
