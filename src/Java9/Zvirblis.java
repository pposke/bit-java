package Java9;

public class Zvirblis extends Paukstis {
    private String pavadinimas;
    private Integer amzius;
    private Double svoris;


    public Zvirblis(String pavadinimas, Integer amzius, Double svoris) {
        this.pavadinimas = pavadinimas;
        this.amzius = amzius;
        this.svoris = svoris;
    }

    @Override
    public String gautiPavadinima() {
        return pavadinimas;
    }

    @Override
    public Integer gautiGyvenimoAmziu() {
        return amzius;
    }

    @Override
    public Double gautiSvori() {
        return svoris;
    }
}