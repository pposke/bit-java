package Java9;

public abstract class Paukstis {

    public abstract String gautiPavadinima();
    public abstract Integer gautiGyvenimoAmziu();
    public abstract Double gautiSvori();
}
