package Java7;

class Darbuotojas {
    private String vardas;
    private String pavarde;
    private Integer amzius;
    private String pareigybes;

    public Darbuotojas( String vardas, String pavarde, Integer amzius, String pareigybes){
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
        this.pareigybes = pareigybes;

    }

    // kitas konstruktorius, kuriame priskiariame kitoki kintamaji
    public Darbuotojas(String vardas, String pavarde, Integer amzius){
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
        this.pareigybes = "Pasalpinis";
    }

    // To string, kaip atspausdinti patogiau. vietoje heroglifu atspausdina visa eilute, nereikia visur get aprasyti

    public String toString(){
        return "Vardas: " + vardas + " Pavarde: " + pavarde + " amzius: " + amzius + " pareigybes: " + pareigybes;
    }


    // cia apsiraso getteris, get
    public String getVardas(){
        return vardas;
    }
    // setteris apsiraso, kaip metodas - su void
    public void setVardas(String vardas){
        this.vardas = vardas;
    }
    // cia apsiraso getteris, get
    public String getPavarde(){
        return pavarde;
    }
    // setteris apsiraso, kaip metodas - su void
    public void setPavarde(String pavarde){
        this.pavarde = pavarde;
    }
    // cia apsiraso getteris, get
    public Integer getAmzius(){
        return amzius;
    }
    // setteris apsiraso, kaip metodas - su void
    public void setAmzius(Integer amzius){
        this.amzius = amzius;
    }
    // cia apsiraso getteris, get
    public String getPareigybes(){
        return pareigybes;
    }
    // setteris apsiraso, kaip metodas - su void
    public void setPareigybes(String pareigybes){
        this.pareigybes = pareigybes;
    }


}
