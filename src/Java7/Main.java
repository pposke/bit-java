package Java7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {
    public static void main(String[] args) {

        // kuriames objekta, pagal kuri atspausdinsime rezultata

//        Darbuotojas darbuotojas = new Darbuotojas("Jonas", "Jonaitis", 25, "kasininkas");
//        System.out.println(darbuotojas);
//
//        darbuotojas.setVardas("Gaidys");
//        System.out.println(darbuotojas);
//        darbuotojas.setAmzius(99);
//        System.out.println(darbuotojas);
//        // atspausdina antra konstruktoriu
//        Darbuotojas darbuotojas2 = new Darbuotojas("andrius", "petraitis", 666);
//        System.out.println(darbuotojas2);

        String failoKelias = new File("").getAbsolutePath()
                + "/src/Java7/Duomenys.txt";

        Darbuotojas[] darbuotojai = skaitymas(failoKelias);

        spausdinamDarbuotojus(darbuotojai);

    }

    // metodas, kuris atspausdina
    public static void spausdinamDarbuotojus( Darbuotojas[] darbuotojai) {
        for (int i = 0; i < darbuotojai.length; i++) {
            System.out.println(darbuotojai[i]);
        }
    }
    // skaitymas is duoemnu failo
    public static Darbuotojas[] skaitymas (String failoKelias) {
        Darbuotojas[] darbuotojai = new Darbuotojas[3];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            // susikuriam indeksa su reiksmia
            Integer indeksas = 0;

            while (eilute != null) {
                // splitiniam, kad suprastu, jog tia yra atskiri duomenys
                String[] eilDuomenys = eilute.split(" ");
                // pasiema duomenys. VISUS PIRMOS EILUTES DUOMENYS
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                // parsint pakeicia is Integerio i stringa
                Integer amzius = Integer.parseInt(eilDuomenys[2]);
                String pareigos = eilDuomenys[3];
                // sukuriam objekta ir priskiriam reiksmias
                Darbuotojas objektas = new Darbuotojas(vardas, pavarde, amzius, pareigos);
                // idedam i masyva.
                darbuotojai[indeksas] = objektas;
                indeksas++;
                // nuskaitom sekancia eilute
                eilute = skaitytuvas.readLine();
            }


        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return darbuotojai;

    }
}
