package DidysisND;

public class Skalbykle extends Preke {
    String skalbTalposKiekis;

    public Skalbykle(Integer prekesId, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double vienetoKaina, String skalbTalposKiekis) {
        super(prekesId, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina);
        this.skalbTalposKiekis = skalbTalposKiekis;
    }

    @Override
    public String toString() {
        return   getPrekesId() + " " + getPrekesTipas() + " " + getPrekesPavadinimas() + " " + getPrekesKiekis() + " " + getVienetoKaina() + " " + skalbTalposKiekis  +"\n";
    }

    public String getSkalbTalposKiekis() {
        return skalbTalposKiekis;
    }

    public void setSkalbTalposKiekis(String skalbTalposKiekis) {
        this.skalbTalposKiekis = skalbTalposKiekis;
    }
}
