package DidysisND;

public class Televizorius extends Preke {
    //Televizoriai – technologija, raiška

    public String technologija;
    public String raiska;


    @Override
    public String toString() {
        return
                getPrekesId() + " " + getPrekesTipas() + " " + getPrekesPavadinimas() + " " + getPrekesKiekis() + " " + getVienetoKaina() + " " + technologija + " " + raiska  +"\n";
    }

    public Televizorius(Integer prekesId, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double vienetoKaina, String technologija, String raiska) {
        super(prekesId, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina);
        this.technologija = technologija;
        this.raiska = raiska;
    }

    public String getTechnologija() {
        return technologija;
    }

    public void setTechnologija(String technologija) {
        this.technologija = technologija;
    }

    public String getRaiska() {
        return raiska;
    }

    public void setRaiska(String raiska) {
        this.raiska = raiska;
    }
}
