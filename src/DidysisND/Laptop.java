package DidysisND;

public class Laptop extends Preke {
    // Nesiojamas kompiuteris – procesorius, ram atminties dydis, disko talpa
    public String procerius;
    public String ram;
    public String diskoTalpa;

    @Override
    public String toString() {
        return    getPrekesId() + " " +
                 getPrekesTipas() + " " + getPrekesPavadinimas() +" " + getPrekesKiekis() + " " +
                getVienetoKaina() + " " + procerius + " " + ram + " " + diskoTalpa +"\n";
    }

    public Laptop(Integer prekesId, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double vienetoKaina, String procerius, String ram, String diskoTalpa) {
        super(prekesId, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina);
        this.procerius = procerius;
        this.ram = ram;
        this.diskoTalpa = diskoTalpa;
    }

    public String getProcerius() {
        return procerius;
    }

    public void setProcerius(String procerius) {
        this.procerius = procerius;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getDiskoTalpa() {
        return diskoTalpa;
    }

    public void setDiskoTalpa(String diskoTalpa) {
        this.diskoTalpa = diskoTalpa;
    }
}
