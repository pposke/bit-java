package DidysisND;



import Krepsinis.Zaidejas;

import java.io.*;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/DidysisND/prekes.txt";
        String defaultPrekiuFailas = new File("").getAbsolutePath()
                + "/src/DidysisND/prekesdefault.txt";

        List<Preke> defaultPrekes = skaitymas(defaultPrekiuFailas);
        List<Preke> visosPrekes = skaitymas(failoKelias);

        System.out.println("Įveskite savo pasirinkimą");
        System.out.println("Pasirinkimas 1 suranda prekę, kurios yra daugiausiai");
        System.out.println("Pasirinkimas 2 suranda prekę , kurios yra daugiausiai");
        System.out.println("Pasirinkimas 3 Išfiltruoja prekes pagal tipa ir įrašo į faila Afiltruota");
        System.out.println("Pasirinkimas 4 Prideda jūsų suvesta prekę pagal tipą");
        System.out.println("Pasirinkimas 5 pasirinkite prekės ID, kuria norėtumėt ištrinti");
        System.out.println("Pasirinkimas 6 galite readaguoti norimą prekę pagal jos ID");
        System.out.println("Pasirinkimas 7 ištrina visas prekes ir gražina seną sąrašą");

        menu(visosPrekes, failoKelias, defaultPrekes);


    }


    public static ArrayList<Preke> skaitymas(String failas) {
        ArrayList<Preke> objektuMasyvas = new ArrayList<>();
        String[] objektas = null;
        try (BufferedReader br = new BufferedReader(new FileReader(failas))) {
            String line = br.readLine();
            while (line != null) {
                objektas = line.split(" ");
                Integer prekesID = Integer.parseInt(objektas[0]);
                String prekesTipas = objektas[1];
                String prekesPavadinimas = objektas[2];
                Integer prekesKiekis = Integer.parseInt(objektas[3]);
                Double vienetoKaina = Double.parseDouble(objektas[4]);

                if (objektas[1].equals("televizoriai")) {
                    String raiska = objektas[6];
                    String techologija = objektas[5];
                    Televizorius tv = new Televizorius(prekesID, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina, techologija, raiska);
                    objektuMasyvas.add(tv);


                } else if (objektas[1].equals(("nesiojamas-kompiuteris"))) {
                    String procesorius = objektas[5];
                    String ram = objektas[6];
                    String diskoTalpa = objektas[7];
                    Laptop pc = new Laptop(prekesID, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina, procesorius, ram, diskoTalpa);
                    objektuMasyvas.add(pc);


                } else if (objektas[1].equals("skalbykle")) {
                    String skalbTalposKiekis = objektas[5];

                    Skalbykle skalb = new Skalbykle(prekesID, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina, skalbTalposKiekis);
                    objektuMasyvas.add(skalb);

                } else if (objektas[1].equals("dziovykle")) {
                    String talposKiekis = objektas[5];
                    Dziovykle dziov = new Dziovykle(prekesID, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina, talposKiekis);
                    objektuMasyvas.add(dziov);
                }
                line = br.readLine();


            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("something else went wrong");
        }
        return objektuMasyvas;
    }


   // Prekes istrinimas
    public static void istrintiPreke(List<Preke> visosPrekes, String failoKelias, Integer id) {
        for (int i = 0; i < visosPrekes.size(); i++) {
            if (visosPrekes.get(i).getPrekesId().equals(id)) {
                visosPrekes.remove(visosPrekes.get(i));

            }

        }
        System.out.println("ISTRINTA PREKE +++++++" + "\n" + visosPrekes);


    }

    // Atrinkimas pagal prekiu tipa
    public static void prekiuTipai (List<Preke> visosPrekes, String atfiltruotosPrekes) {
        List<Preke> atrinktiTipai = new ArrayList<>();
        for (int i = 0; i < visosPrekes.size(); i++) {
            if (visosPrekes.get(i).getPrekesTipas().equals("televizoriai")) {
                atrinktiTipai.add(visosPrekes.get(i));

            }
        }
        rasyti(atfiltruotosPrekes,atrinktiTipai);

    }

    // Prekes ieskojimas, kurios yra daugiausiai
    public static Preke didziausiasVnt(List<Preke> vnt) {
        Integer indeksas = 0;
        Integer didziausias = vnt.get(0).getPrekesKiekis();
        for (int i = 0; i < vnt.size(); i++) {
            if (vnt.get(i).getPrekesKiekis() > didziausias) {
                didziausias = vnt.get(i).getPrekesKiekis();
                indeksas = i;
            }
        }
        return vnt.get(indeksas);
    }
    // Prekes ieskojimas, kurios yra maziausiai
    public static Preke maziausiaiVnt(List<Preke> vnt) {
        Integer indeksas = 0;
//        List<Prekes> maziuasiaiVnt = new ArrayList<>();
        Integer maziausias = vnt.get(0).getPrekesKiekis();
        for (int i = 0; i < vnt.size(); i++) {
            if (vnt.get(i).getPrekesKiekis() < maziausias) {
                maziausias = vnt.get(i).getPrekesKiekis();
                indeksas = i;

            }
        }
        return vnt.get(indeksas);
    }

    // rasymas i faila
    public static void rasyti(String rezultatai, List<Preke> prekes) {
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatai))) {

            for (Preke preke : prekes) {
                spausdinimas.write(preke.toString());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ieskojimas prekes pagal ID
    public static Integer gautiPrekePagalID(List<Preke> prekes,
                                            Integer id) {
        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getPrekesId().equals(id)) {
                return i;
            }
        }
        return null;
    }


    public static Integer skaiciuScanneris(String zinute, Scanner skaitytuvas) {
        System.out.println(zinute);
        Integer skaicius = null;
        try {
            skaicius = skaitytuvas.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Ivestas blogas formatas");
            skaitytuvas.next();
            skaicius = skaiciuScanneris(zinute, skaitytuvas);
        }
        return skaicius;
    }



    // MENU
    public static void menu(List<Preke> visosPrekes, String failoKelias, List<Preke> defaultPrekes) {
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                // Perform "original number" case.
                System.out.println("Maziausiai vnt turinti preke");
                System.out.println(maziausiaiVnt(visosPrekes));
                choice = scanner.nextInt();
                break;

            case 2:
                System.out.println("Daugiausiai vnt turinti preke");
                System.out.println(didziausiasVnt(visosPrekes));
                choice = scanner.nextInt();
                break;

            case 3:
                String atfiltruotosPrekes = new File("").getAbsolutePath()
                        + "/src/DidysisND/Atfiltruota.txt";
                prekiuTipai(visosPrekes,atfiltruotosPrekes);
                choice = scanner.nextInt();
                break;

            case 4:
                Scanner skaitytuvas = new Scanner(System.in);
                System.out.println("Pasirinkite koki tipa norite prideti");
                System.out.println("1 - skalbykle, 2 - Dziovykle, 3 - kompiuteris, 4 - televizorius");
                Integer pasirinkimas = skaitytuvas.nextInt();
                String tipas;
                System.out.println("Iveskite gamintojo pavadinima");
                String pavadinimas = skaitytuvas.next();
                System.out.println("Iveskite kieki");
                Integer kiekis = skaitytuvas.nextInt();
                System.out.println("Iveskite vnt. kaina");
                Double vntKaina = skaitytuvas.nextDouble();
                if (pasirinkimas == 1) {
                    tipas = "skalbykle";
                    System.out.println("Iveskite skalbykles talpa");
                    String talpa = skaitytuvas.next();
                    Skalbykle naujaSkalbykle = new Skalbykle(1, tipas, pavadinimas, kiekis, vntKaina, talpa);
                    visosPrekes.add(naujaSkalbykle);
                    rasyti(failoKelias, visosPrekes);
                    System.out.println("Preke buvo sekmingai prideta prie sąrašo");
                    // panaudoti ta pati rasymo metoda
                } else if (pasirinkimas == 2) {
                    tipas = "dziovykle";
                    System.out.println("Iveskite dziovykles talpa");
                    String talpa = skaitytuvas.next();
                    Dziovykle naujaDziovykle = new Dziovykle(6, tipas, pavadinimas, kiekis, vntKaina, talpa);
                    visosPrekes.add(naujaDziovykle);
                    rasyti(failoKelias, visosPrekes);
                    System.out.println("Preke buvo sekmingai prideta prie sąrašo");
                } else if (pasirinkimas == 3) {
                    tipas = "kompiuteris";
                    System.out.println("Iveskite kompiuterio procesoriu");
                    String procesorius = skaitytuvas.next();
                    System.out.println("Iveskite kompiuterio ram");
                    String ram = skaitytuvas.next();
                    System.out.println("Iveskite disko talpa");
                    String diskoTalpa = skaitytuvas.next();
                    Laptop naujasKompiuteris = new Laptop(3, tipas, pavadinimas, kiekis, vntKaina, procesorius, ram, diskoTalpa);
                    visosPrekes.add(naujasKompiuteris);
                    rasyti(failoKelias, visosPrekes);
                    System.out.println("Preke buvo sekmingai pridėta prie sąrašo");

                } else if (pasirinkimas == 4) {
                    tipas = "televizorius";
                    System.out.println("Iveskite technologija");
                    String technologija = skaitytuvas.next();
                    System.out.println("Iveskite raiska");
                    String raiska = skaitytuvas.next();
                    Televizorius naujasTv = new Televizorius(4, tipas, pavadinimas, kiekis, vntKaina, technologija, raiska);
                    visosPrekes.add(naujasTv);
                    rasyti(failoKelias, visosPrekes);
                    System.out.println("Preke buvo sekmingai pridėta prie sąrašo");
                }
                rasyti(failoKelias,visosPrekes);
                choice = scanner.nextInt();
                break;
            case 5:
                Scanner trintuvas = new Scanner(System.in);
                System.out.println("iveskite prekes id, kuria norite istrinti");
                Integer id = trintuvas.nextInt();
                istrintiPreke(visosPrekes, failoKelias, id);
                rasyti(failoKelias, visosPrekes);
                System.out.println("Preke buvo sėkmingai ištrinta");
                rasyti(failoKelias,visosPrekes);
                choice = scanner.nextInt();
                break;
            case 6:
//                List<Preke> prekes = new ArrayList<>();
                Scanner skaitytuvas2 = new Scanner(System.in);
                System.out.println("Pasirinkite preke kuria norite redaguoti");
                System.out.println("5 - skalbykle, 6 - Dziovykle, 3,4 - kompiuteris, 1,2 - televizorius");

                Integer pasirinkimas2 = skaitytuvas2.nextInt();
                Integer prekePagalIDIndeksas = gautiPrekePagalID(visosPrekes, pasirinkimas2);
                if (prekePagalIDIndeksas == 1 || prekePagalIDIndeksas == 2) {
                    System.out.println("Iveskite naują prekės tipą");
                    String tipas2 = skaitytuvas2.next();
                    System.out.println("Iveskite naują prekės pavadinima");
                    String pavadinimas2 = skaitytuvas2.next();
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesPavadinimas(pavadinimas2);
                    Integer kiekis2 = skaiciuScanneris("Iveskite nauja kieki", skaitytuvas2);
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesKiekis(kiekis2);
                    System.out.println("Iveskite nauja kaina");
                    Double kainaVnt = skaitytuvas2.nextDouble();
                    visosPrekes.get(prekePagalIDIndeksas).setVienetoKaina(kainaVnt);
                    System.out.println("Iveskite nauja technologija");
                    String technologijaNauja = skaitytuvas2.next();
                    System.out.println("Iveskite nauja raiska");
                    String raiska2 = skaitytuvas2.next();
                    Televizorius naujasTv = new Televizorius(prekePagalIDIndeksas, tipas2, pavadinimas2, kiekis2, kainaVnt, technologijaNauja, raiska2);
                    visosPrekes.add(naujasTv);


                    rasyti(failoKelias,visosPrekes);
                } else if (prekePagalIDIndeksas == 3 || prekePagalIDIndeksas == 4) {
                    System.out.println("Iveskite naują prekės tipą");
                    String tipas2 = skaitytuvas2.next();
                    System.out.println("Iveskite naują prekės pavadinima");
                    String pavadinimas2 = skaitytuvas2.next();
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesPavadinimas(pavadinimas2);
                    Integer kiekis2 = skaiciuScanneris("Iveskite nauja kieki", skaitytuvas2);
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesKiekis(kiekis2);
                    System.out.println("Iveskite nauja kaina");
                    Double kainaVnt = skaitytuvas2.nextDouble();
                    visosPrekes.get(prekePagalIDIndeksas).setVienetoKaina(kainaVnt);
                    System.out.println("Iveskite nauja procesoriu");
                    String procesorius = skaitytuvas2.next();
                    System.out.println("Iveskite naujus rma");
                    String ram = skaitytuvas2.next();
                    System.out.println("Iveskite nauja diskotalpa");
                    String diskoTalpa = skaitytuvas2.next();
                    Laptop naujasLaptop = new Laptop(prekePagalIDIndeksas, tipas2, pavadinimas2, kiekis2, kainaVnt, procesorius, ram, diskoTalpa);
                    visosPrekes.add(naujasLaptop);

                } else if (prekePagalIDIndeksas == 5) {
                    System.out.println("Iveskite naują prekės tipą");
                    String tipas2 = skaitytuvas2.next();
                    System.out.println("Iveskite naują prekės pavadinima");
                    String pavadinimas2 = skaitytuvas2.next();
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesPavadinimas(pavadinimas2);
                    Integer kiekis2 = skaiciuScanneris("Iveskite nauja kieki", skaitytuvas2);
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesKiekis(kiekis2);
                    System.out.println("Iveskite nauja kaina");
                    Double kainaVnt = skaitytuvas2.nextDouble();
                    visosPrekes.get(prekePagalIDIndeksas).setVienetoKaina(kainaVnt);
                    System.out.println("Iveskite nauja talpa");
                    String talpa = skaitytuvas2.next();
                    Skalbykle naujaSkalbykle = new Skalbykle(prekePagalIDIndeksas, tipas2, pavadinimas2, kiekis2, kainaVnt, talpa);

                } else if (prekePagalIDIndeksas == 6) {
                    System.out.println("Iveskite naują prekės tipą");
                    String tipas2 = skaitytuvas2.next();
                    System.out.println("Iveskite naują prekės pavadinima");
                    String pavadinimas2 = skaitytuvas2.next();
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesPavadinimas(pavadinimas2);
                    Integer kiekis2 = skaiciuScanneris("Iveskite nauja kieki", skaitytuvas2);
                    visosPrekes.get(prekePagalIDIndeksas).setPrekesKiekis(kiekis2);
                    System.out.println("Iveskite nauja kaina");
                    Double kainaVnt = skaitytuvas2.nextDouble();
                    visosPrekes.get(prekePagalIDIndeksas).setVienetoKaina(kainaVnt);
                    System.out.println("Iveskite nauja talpa");
                    String talpa = skaitytuvas2.next();
                    Dziovykle naujaDziovykle = new Dziovykle(prekePagalIDIndeksas, tipas2, pavadinimas2, kiekis2, kainaVnt, talpa);
                    visosPrekes.add(naujaDziovykle);
                } else {
                    System.out.println("tokios prekes nera");
                }
                rasyti(failoKelias,visosPrekes);
                break;

            case 7:
                rasyti(failoKelias,defaultPrekes);
                System.out.println(defaultPrekes);

                break;

            default:
                System.out.println("Visogero");
        }
    }
}






