package DidysisND;

public class Dziovykle extends Preke {
    public String talposKiekis;

    public Dziovykle(Integer prekesId, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double vienetoKaina, String talposKiekis) {
        super(prekesId, prekesTipas, prekesPavadinimas, prekesKiekis, vienetoKaina);
        this.talposKiekis = talposKiekis;
    }

    @Override
    public String toString() {
        return  getPrekesId() + " " + getPrekesTipas() + " " +getPrekesPavadinimas() +" " + getPrekesKiekis() + " " +
                getVienetoKaina() + " " + talposKiekis +"\n";
    }

    public String getTalposKiekis() {
        return talposKiekis;
    }

    public void setTalposKiekis(String talposKiekis) {
        this.talposKiekis = talposKiekis;
    }
}
