package DidysisND;

abstract class Preke {
    private Integer prekesId;
    private String prekesTipas;
    private String prekesPavadinimas;
    private Integer prekesKiekis;
    private Double vienetoKaina;


    public Preke(Integer prekesId, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double vienetoKaina){
        this.prekesId = prekesId;
        this.prekesTipas = prekesTipas;
        this.prekesPavadinimas = prekesPavadinimas;
        this.prekesKiekis = prekesKiekis;
        this.vienetoKaina = vienetoKaina;
    }

    @Override
    public String toString() {
        return  prekesId + " " +
                prekesTipas + " " +
                prekesPavadinimas + " "  + " " +
                prekesKiekis + " " +
                vienetoKaina + "\n";
    }



    public void setPrekesId(Integer prekesId) {
        this.prekesId = prekesId;
    }

    public void setPrekesTipas(String prekesTipas) {
        this.prekesTipas = prekesTipas;
    }

    public void setPrekesPavadinimas(String prekesPavadinimas) {
        this.prekesPavadinimas = prekesPavadinimas;
    }

    public void setPrekesKiekis(Integer prekesKiekis) {
        this.prekesKiekis = prekesKiekis;
    }

    public void setVienetoKaina(Double vienetoKaina) {
        this.vienetoKaina = vienetoKaina;
    }

    public Integer getPrekesId() {
        return prekesId;
    }

    public String getPrekesTipas() {
        return prekesTipas;
    }

    public String getPrekesPavadinimas() {
        return prekesPavadinimas;
    }

    public Integer getPrekesKiekis() {
        return prekesKiekis;
    }

    public Double getVienetoKaina() {
        return vienetoKaina;
    }
}

