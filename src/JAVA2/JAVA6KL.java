package JAVA2;

import java.util.Scanner;

public class JAVA6KL {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite zodi");
        String zodis = reader.next();
        System.out.println("Pirma raide " + zodis.charAt(0));
        System.out.println("Paskutine raide " + zodis.charAt(zodis.length() -1 ));
    }
}
