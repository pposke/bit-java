package JAVA2;

import java.util.Scanner;

public class JAVA3KL {
    public static void main(String[] args) {
      Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite skritulio spinduli");
        Integer r = reader.nextInt();
        Double apskritimoPlotas = Math.PI * Math.pow(r,2);


        String rezultatas = String.format("%.2f", apskritimoPlotas);
        System.out.println(rezultatas);

    }
}
