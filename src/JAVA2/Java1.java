package JAVA2;

import java.util.Scanner;

public class Java1 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite kvadrato krastine a");
        Integer a = reader.nextInt();
        System.out.println("Iveskite kvadrato krastine b");
        Integer b = reader.nextInt();

        Integer plotas = a * b / 2;
        System.out.println("Staciojo trikampio plotas " + plotas);
    }
}
