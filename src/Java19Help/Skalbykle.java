package Java19Help;

public class Skalbykle extends Prekes {
    private String talpa;

    public Skalbykle(Integer id, String tipas, String pavadinimas, Integer kiekis, Double vntKaina, String talpa) {
        super(id, tipas, pavadinimas, kiekis, vntKaina);
        this.talpa = talpa;
    }

    @Override
    public String toString() {
        return super.toString() + " " + getTalpa();
    }

    public String getTalpa() {
        return talpa;
    }

    public void setTalpa(String talpa) {
        this.talpa = talpa;
    }
}

