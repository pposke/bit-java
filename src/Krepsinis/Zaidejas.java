package Krepsinis;

public class Zaidejas {
    private Integer numeris;
    private String vardas;
    private String pavarde;
    private Integer baudos;
    private Integer dvitaskiai;
    private Integer tritaskiai;
    private Integer taskai;
    private Integer nepataikyti;
    private Double tikslumas;


    public Zaidejas(Integer numeris, String vardas
            , String pavarde) {
        this.numeris = numeris;
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.baudos = 0;
        this.dvitaskiai = 0;
        this.tritaskiai = 0;
        this.taskai = 0;
        this.nepataikyti = 0;
        this.tikslumas = 0d;
    }

    @Override
    public String toString() {
        return String.format("|%3d|%12s %12s|%7d|%7d|%7d|%8d|%6d|%9.0f|\n",
                numeris, vardas, pavarde, baudos, dvitaskiai,
                tritaskiai, taskai, nepataikyti, getTikslumas());
    }

    public Integer kiekMesta() {
        return getBaudos() + getDvitaskiai() + getTritaskiai() + getNepataikyti();
    }

    public Integer kiekPataikyta() {
        return getBaudos() + getDvitaskiai() + getTritaskiai();
    }

    public Integer getNumeris() {
        return numeris;
    }

    public void setNumeris(Integer numeris) {
        this.numeris = numeris;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public Integer getBaudos() {
        return baudos;
    }

    public void setBaudos(Integer baudos) {
        this.baudos = baudos;
    }

    public Integer getDvitaskiai() {
        return dvitaskiai;
    }

    public void setDvitaskiai(Integer dvitaskiai) {
        this.dvitaskiai = dvitaskiai;
    }

    public Integer getTritaskiai() {
        return tritaskiai;
    }

    public void setTritaskiai(Integer tritaskiai) {
        this.tritaskiai = tritaskiai;
    }

    public Integer getTaskai() {
        return taskai;
    }

    public void setTaskai(Integer taskai) {
        this.taskai = taskai;
    }

    public Integer getNepataikyti() {
        return nepataikyti;
    }

    public void setNepataikyti(Integer nepataikyti) {
        this.nepataikyti = nepataikyti;
    }

    public Double getTikslumas() {
        Double tikslumas = kiekPataikyta() * 100d / kiekMesta();
        if (Double.isNaN(tikslumas)) {
            return 0d;
        }
        return tikslumas;
    }

    public void setTikslumas(Double tikslumas) {
        this.tikslumas = tikslumas;
    }
}