package Java7nd;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/Java7nd/Studentai.txt";

//        Mokiniai mokiniai = new Mokiniai("Andrius", "Andrelis", 5, " 8 7 8 10 ");
////        System.out.println(mokiniai);

        Mokiniai[] mokiniai = skaitymas(failoKelias);
        spausdintiMokinius(mokiniai);

    }

    public static void spausdintiMokinius( Mokiniai[] mokiniai) {
            for (int i = 0; i < mokiniai.length; i++) {
                System.out.println(mokiniai[i]);
            }

    }
    // cia yra sukuriamas naujas masyvas

    public static Integer[] pridetiMasyva(Integer[] pazymiai, Integer pazymis){
        pazymiai = Arrays.copyOf(pazymiai, pazymiai.length + 1);
        pazymiai[pazymiai.length - 1] = pazymis;
        return pazymiai;
    }
    public static Mokiniai[] skaitymas (String failoKelias) {
        Mokiniai[] mokiniai = new Mokiniai[5];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            // susikuriam indeksa su reiksmia
            Integer indeksas = 0;

            while (eilute != null) {
                // splitiniam, kad suprastu, jog tia yra atskiri duomenys
                String[] eilDuomenys = eilute.split(" ");
                // pasiema duomenys. VISUS PIRMOS EILUTES DUOMENYS
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                // parsint pakeicia is Integerio i stringa
                Integer klase = Integer.parseInt(eilDuomenys[2]);

                Integer[] pazymiai = new Integer[0];
                // sudedu i nauja masyva skaicius
                for (int i = 3; i < eilDuomenys.length; i++) {
                    pazymiai = pridetiMasyva(pazymiai,Integer.parseInt(eilDuomenys[i]));

                }
                System.out.println("cia yra pazymiai" + pazymiai);
                // sukuriam objekta ir priskiriam reiksmias
                Mokiniai objektas = new Mokiniai(vardas, pavarde, klase, pazymiai);
                // idedam i masyva.
                mokiniai[indeksas] = objektas;
                indeksas++;
                // nuskaitom sekancia eilute
                eilute = skaitytuvas.readLine();




            }




        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return mokiniai;

    }

}
