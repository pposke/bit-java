package Java7nd;

public class Mokiniai {
    private String vardas;
    private String pavarde;
    private Integer klase;
    private Integer[] pazymiai;

    // paklausti destytojo, kaip padaryti, kad galima butu surasyti daugiau integeriu - skaiciu
    public Mokiniai (String vardas, String pavarde, Integer klase, Integer[] pazymiai){
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        this.pazymiai = pazymiai;

    }
    public String toString(){
        return "Vardas: " + vardas + " Pavarde: " + pavarde + " Klase: " + klase + " Pazymiai: " + spausdintiPazymius(pazymiai);
    }

    private String spausdintiPazymius(Integer[] pazymiai){
        String pazymiaiSpausdinti = "";
        for (Integer pazymis: pazymiai) {
            pazymiaiSpausdinti += " " + pazymis;
        }
        return pazymiaiSpausdinti;
    }
    public String getVardas(){
        return vardas;
    }
    public void setVardas (String vardas){
        this.vardas = vardas;
    }
    public String getPavarde(){
        return pavarde;
    }
    public void setPavarde(String pavarde){
        this.pavarde = pavarde;
    }
    public Integer getKlase(){
        return klase;
    }
    public void setKlase(Integer klase){
        this.klase = klase;
    }
}


