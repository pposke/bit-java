package JAVA2NamuDarbai;

import java.util.Scanner;

public class Kuras {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Kiek sunaudoja 100km litru");
        Double kiekLitru = reader.nextDouble();
        System.out.println("Kiek kilometru?");
        Double kiekKm = reader.nextDouble();
        System.out.println("Kiek zmoniu vaziavo?");
        Double kiekZmoniu = reader.nextDouble();
        System.out.println("Kiek kainuoja kuras?");
        Double kuroKaina = reader.nextDouble();

        Double rezultatas = (kiekKm * kiekLitru / 100) * kuroKaina / kiekZmoniu;
        String galutinis = String.format("%.2f", rezultatas);
        System.out.println(galutinis);

    }
}
