package JAVA2NamuDarbai;

import java.util.Scanner;

public class Treniruotes {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Treniruotes trukme pirmadieni");
        Double pirm = reader.nextDouble();
        System.out.println("Treniruotes trukme antradieni");
        Double antr = reader.nextDouble();
        System.out.println("Treniruotes trukme treciadieni");
        Double trec = reader.nextDouble();
        System.out.println("Treniruotes trukme ketvirtadieni");
        Double ketv = reader.nextDouble();
        System.out.println("Treniruotes trukme penktadieni");
        Double penkt = reader.nextDouble();
        Double viso = (pirm + antr + trec + ketv + penkt) * 60;
        System.out.println("Per savaite andrius treniruojasi " + viso + " minuciu");
    }
}
