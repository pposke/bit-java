package JAVA2NamuDarbai;

import java.util.Scanner;

public class VanduoKubai {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Kiek vandes saugykloje yra ?");
        Double vandensSaug = reader.nextDouble();
        System.out.println("Kiek zmoniu vartoja vandeni?");
        Double kiekZmoniu = reader.nextDouble();
        System.out.println("Kiek vienas zmogus suvartoja vadens");
        Double kiekSuvartoja = reader.nextDouble();

        Double rezultatas = vandensSaug / (kiekZmoniu*kiekSuvartoja);
        System.out.println(rezultatas);
    }
}
