package Java1;

import java.util.Scanner;

public class ArLyginisSkaicius {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite skaicius: ");
        int skaicius = skaitytuvas.nextInt();
        skaitytuvas.close();
        if (skaicius == 0){
            System.out.println("Skaicius yra lygus nuliui");
        }else if (skaicius % 2 == 0){
            System.out.println("Skaicius yra lyginis");
        } else {
            System.out.println("Skaicius yra nelyginis");
        }

    }
}
