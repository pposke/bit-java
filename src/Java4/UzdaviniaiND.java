package Java4;

import java.io.*;

public class UzdaviniaiND {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java4/duomenys.txt";
        String rezultatuKelias = new File("").getAbsolutePath()
                + "/src/java4/rezultatai.txt";
        Integer[] skaiciai = null;

        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            String[] eilReiksmes = eilute.split(" ");
            Integer eiluciuKiekis = Integer.parseInt(eilReiksmes[0]);
            Integer elementuKiekis = Integer.parseInt(eilReiksmes[1]);
            skaiciai = new Integer[elementuKiekis];
            Integer indeksas = 0;
            for (int i = 0; i < eiluciuKiekis; i++) {
                eilute = skaitytuvas.readLine();
                eilReiksmes = eilute.split(" ");

                for(int j = 0; j < eilReiksmes.length; j++) {
                    skaiciai[indeksas] = Integer.parseInt(eilReiksmes[j]);
                    indeksas++;
                }

            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        for(Integer skaicius: skaiciai) {
            System.out.println(skaicius);
        }

        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatuKelias))) {
            //0 uzduotis atspausdina tiesiog duomenys vienoje eiluteje
            for (int i =0; i < skaiciai.length; i++){
                spausdinimas.write(skaiciai[i] + " ");


            }
            // 1 uzduotis atspausdina atvirskciai ---------------------------
//            for (int i = skaiciai.length-1; i>=0; i--){
//                spausdinimas.write( + skaiciai[i] + " ");
//            }
            // 2 uzduotis atspausdina vidurki ---------------------
            Integer suma = 0;
            for(int i=0; i < skaiciai.length ; i++){
                suma += skaiciai[i];
        }


            Integer kiekis = skaiciai.length;
            Double vidurkis = (double) suma / kiekis;
            spausdinimas.write("\n" + "vidurkis = " + vidurkis + "\n");



            // 3 uzduotis pasalinti visus skaicius mazesnius uz vidurki

            Integer mazesniuKiekis = 0;
            for (int i = 0; i < skaiciai.length; i++){
                if (skaiciai[i] < vidurkis){
                    mazesniuKiekis++;
                }
            }
            Integer[] mazesniuMasyvas = new Integer[skaiciai.length - mazesniuKiekis];
            int index = 0;
            for (int i = 0; i < skaiciai.length; i++) {
                if (skaiciai[i] < vidurkis) {
                    continue;
                }
                mazesniuMasyvas[index++] = skaiciai[i];
            }
                for (int i = 0; i < mazesniuMasyvas.length; i++){
                    spausdinimas.write(mazesniuMasyvas[i] + " ");


            }

            Integer max = 0;
                for (int i = 0; i < skaiciai.length;i++ ){
                    if (max < skaiciai[i]){
                        max = skaiciai[i];
                    }
                }
            System.out.println("didziausia masyvo reiksme = " + max);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
