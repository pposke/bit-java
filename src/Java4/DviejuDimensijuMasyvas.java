package Java4;


public class DviejuDimensijuMasyvas {

    public static void main(String[] args) {
        Integer[][] matrica = new Integer[2][2];
        matrica[0][0] = 1;
        matrica[0][1] = 5;
        matrica[1][0] = 1;
        matrica[1][1] = 9;

//        for (int i = 0; i < matrica.length; i++) {
//            for (int j = 0; j < matrica[i].length; j++) {
//                System.out.println("i = " + i + " j = " + j + " skaicius = " + matrica[i][j]);
//            }
//        }


        Integer[][] matrica2 = {
                {1, 2},
                {3, 4, 5},
                {2, 9, 8, 6}
        };

        for (int i = 0; i < matrica2.length; i++) {
            for (int j = 0; j < matrica2[i].length; j++) {
                System.out.println("i = " + i + " j = " + j + " skaicius = " + matrica2[i][j]);
            }
        }
    }
}
