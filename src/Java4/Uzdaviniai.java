package Java4;

import java.io.*;

public class Uzdaviniai {

    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java4/duomenys.txt";
        String rezultatuKelias = new File("").getAbsolutePath()
                + "/src/java4/rezultatai.txt";
        Integer[] skaiciai = null;

        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            String[] eilReiksmes = eilute.split(" ");
            Integer eiluciuKiekis = Integer.parseInt(eilReiksmes[0]);
            Integer elementuKiekis = Integer.parseInt(eilReiksmes[1]);
            skaiciai = new Integer[elementuKiekis];



            Integer indeksas = 0;
            for (int i = 0; i < eiluciuKiekis; i++) {
                eilute = skaitytuvas.readLine();
                eilReiksmes = eilute.split(" ");

                for(int j = 0; j < eilReiksmes.length; j++) {
                    skaiciai[indeksas] = Integer.parseInt(eilReiksmes[j]);
                    indeksas++;
                }

            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        for(Integer skaicius: skaiciai) {
            System.out.println(skaicius);
        }
        Integer eilSkaicius = 0;
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatuKelias))) {
            for (int i =0; i < skaiciai.length; i++){
                if (eilSkaicius == 4){
                    spausdinimas.write("\n");
                    eilSkaicius = 0;
                }
                eilSkaicius++;
                spausdinimas.write(skaiciai[i] + " ");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
