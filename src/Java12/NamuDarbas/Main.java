package Java12.NamuDarbas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String duomenuFailoKelias = new File("").getAbsolutePath() +
                "/src/Java12/NamuDarbas/Duomenys.txt";

        Masina[] duomenys = skaitymas(duomenuFailoKelias);
        System.out.println(Arrays.toString(duomenys));
        System.out.println("Seniausia masina: "
                + rastiSeniausiaMasina(duomenys));
        System.out.println("VW masinos = ");
        System.out.println(Arrays.toString(rastiVWMasinas(duomenys)));
    }

    public static Masina rastiSeniausiaMasina(Masina[] masyvas) {
        Integer minMetai = 2020;
        Masina obj = null;
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i].getMetai() < minMetai) {
                minMetai = masyvas[i].getMetai();
                obj = masyvas[i];
            }
        }
        return obj;
    }

    public static Masina[] rastiVWMasinas(Masina[] masyvas) {
        Masina[] vwMasinos = new Masina[0];
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i].getGamintojas().equals("VW")) {
                vwMasinos = pridetiElementa(vwMasinos, masyvas[i]);
            }
        }
        return vwMasinos;
    }

    public static Masina[] pridetiElementa(Masina[] masyvas, Masina reiksme) {
        Masina[] naujasMasyvas = Arrays.copyOf(masyvas,
                masyvas.length + 1);
        naujasMasyvas[naujasMasyvas.length - 1] = reiksme;
        return naujasMasyvas;
    }


    public static Masina[] skaitymas(String file) {
        Masina[] masyvas = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine(); // Nuskaito pirma eilute
            Integer kiekMasinu = Integer.parseInt(eilute);
            masyvas = new Masina[kiekMasinu];

            for (int i = 0; i < kiekMasinu; i++) {
                eilute = skaitytuvas.readLine();
                String[] reiksmes = eilute.split(" ");
                String gamintojas = reiksmes[0];
                String modelis = reiksmes[1];
                Integer metai = Integer.parseInt(reiksmes[2]);
                Double kaina = Double.parseDouble(reiksmes[3]);
                Double variklioTuris = Double.parseDouble(reiksmes[4]);
                String kuroTipas = reiksmes[5];

                masyvas[i] = new Masina(gamintojas, modelis, metai, kaina, variklioTuris, kuroTipas);


            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        return masyvas; // grazinam sukurta objekta
    }

}
