package Java12.NamuDarbas;

import Java14.Car;

public class Masina implements Comparable<Masina> {
    private String gamintojas;
    private String modelis;
    private Integer metai;
    private Double kaina;
    private Double variklioTuris;
    private String kuroTipas;

    public Masina(String gamintojas, String modelis, Integer metai, Double kaina,
                  Double variklioTuris, String kuroTipas) {
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.metai = metai;
        this.kaina = kaina;
        this.variklioTuris = variklioTuris;
        this.kuroTipas = kuroTipas;
    }

    public int compareTo(Masina obj){
        int comp = getMetai().compareTo(obj.getMetai());
        if (comp != 0){
            return comp;
        }
        return getModelis().compareTo((obj.getModelis())) ;

    }
    public String toString() {
        return "Gamintojas: " + this.gamintojas + " Modelis: " +
                this.modelis + " Metai: " + this.metai + " Kaina:" +
                this.kaina + " Variklio Turis: " + this.variklioTuris +
                " Kuro tipas: " + this.kuroTipas;
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public void setModelis(String modelis) {
        this.modelis = modelis;
    }

    public Integer getMetai() {
        return metai;
    }

    public void setMetai(Integer metai) {
        this.metai = metai;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }

    public Double getVariklioTuris() {
        return variklioTuris;
    }

    public void setVariklioTuris(Double variklioTuris) {
        this.variklioTuris = variklioTuris;
    }

    public String getKuroTipas() {
        return kuroTipas;
    }

    public void setKuroTipas(String kuroTipas) {
        this.kuroTipas = kuroTipas;
    }
}
