package Java12;

public class ObjetBendras {
    private Object objektas;

    public Object get() {
        return objektas;
    }
    public void set(Object objektas) {
        this.objektas = objektas;
    }
    public static void main(String args[]) {
        ObjetBendras sa = new ObjetBendras();
        sa.set(1235.2);
        Double str = (Double)sa.get(); // reikia kastingo  !!
        sa.set("Labas");
        System.out.println(sa.get());
    }
}
