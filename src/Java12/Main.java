package Java12;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Spalvos spalva = Spalvos.Zalia;
        System.out.println(spalva);
        System.out.println("zaliaa");
        Integer[] masyvas = {1, 2, 3, 4, 5};
        int kiekis = reikšmiųKiekis(masyvas, 1);
        System.out.println(kiekis);
        List<Integer> skaiciai = new ArrayList<>();
        skaiciai.add(1);
        skaiciai.add(2);
        double suma = suma(skaiciai);
        System.out.println(suma);


//        BendrineKlase<Integer> sa = new BendrineKlase<>();
        KeliBendriniaiTipai<String, Integer> obj =
                new KeliBendriniaiTipai<>("Raktas", 123);
        System.out.println(obj.gautiRakta());
        System.out.println(obj.gautiReiksme());
//        T[] x = new T[100];

    }

    public static <T> int reikšmiųKiekis(T[] visos, T reikšmė) {
        int kiekis = 0;
        if (reikšmė == null) {
            for (T eilinė : visos)
                if (eilinė == null) kiekis++;
        } else {
            for (T eilinė : visos)
                if (reikšmė.equals(eilinė)) kiekis++;
        }
        return kiekis;
    }

    public static double suma(List<? extends Number> list) {
        double s = 0.0;
        for (Number n : list)
            s += n.doubleValue();
        return s;
    }


}
