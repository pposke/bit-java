package Java12.uzduotis;

public class Uzduotis {
    public static void main(String[] args) {
        Double[] doublai = {1.0, 2.0, 3.0, 4.0};
        Integer[] skaiciai = {1, 2, 3, 4};
        Character[] charai = {'a', 'b', 'c', 'd'};
        spausdinti(doublai);
        spausdinti(skaiciai);
        spausdinti(charai);
    }

    public static <T> void spausdinti(T[] masyvas) {
        for(T reiksme: masyvas) {
            System.out.print(reiksme + ", ");
        }
        System.out.println();
    }

    private static <E> void swap(E[] a, int i, int j) {
        if (i != j) {
            E temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
    }

    public static <E extends Comparable<E>> void selectionSort(E[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            // find index of smallest element
            int smallest = i;
            for (int j = i + 1; j < a.length; j++) {
                if (a[j].compareTo(a[smallest]) <= 0) {
                    smallest = j;
                }
            }
            swap(a, i, smallest);  // swap smallest to front
        }
    }
}


