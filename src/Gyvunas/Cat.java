package Gyvunas;

public class Cat extends Animal{
    private String garsas;


    public Cat(String vardas, String garsas) {
        super(vardas, garsas);
        this.garsas = garsas;
    }

    public String toString() {
        return "Gyvunas " + getVardas() + " Garsas: " + getGarsas();
    }




}

