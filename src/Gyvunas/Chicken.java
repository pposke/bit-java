package Gyvunas;

public class Chicken extends Animal {



    public Chicken(String vardas, String garsas) {
        super(vardas, garsas);

    }

    public String toString() {
        return "Gyvunas " + getVardas() + " Garsas: " + getGarsas();
    }
}
