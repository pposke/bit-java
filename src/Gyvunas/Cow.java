package Gyvunas;

public class Cow extends Animal{



    public Cow(String vardas, String garsas) {
        super(vardas, garsas);

    }

    public String toString() {
        return "Gyvunas " + getVardas() + " Garsas: " + getGarsas();
    }
}
