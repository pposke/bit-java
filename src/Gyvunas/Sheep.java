package Gyvunas;

public class Sheep extends Animal {


    public Sheep(String vardas, String garsas) {
        super(vardas, garsas);

    }

    public String toString() {
        return "Gyvunas " + getVardas() + " Garsas: " + getGarsas();
    }
}
