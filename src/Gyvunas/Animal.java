package Gyvunas;

public class Animal {

    private String vardas;
    private String garsas;


    public Animal(String vardas, String garsas) {
        this.vardas = vardas;
        this.garsas = garsas;
    }


    public String getVardas() {
        return vardas;
    }

    public String getGarsas(){
        return garsas;
    }
}
