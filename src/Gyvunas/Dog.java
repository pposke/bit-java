package Gyvunas;

public class Dog extends Animal{


    public Dog(String vardas, String garsas) {
        super(vardas, garsas);

    }

    public String toString() {
        return "Gyvunas " + getVardas() + " Garsas: " + getGarsas();
    }
}
