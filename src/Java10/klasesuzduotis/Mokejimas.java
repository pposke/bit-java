package Java10.klasesuzduotis;

public interface Mokejimas {

    String gautiBankoSaskaita();
    String gautiSaskaitosAsmensVarda();
    Double gautiSuma();
    Double[] gautiSumas();
}
