package Java10.klasesuzduotis;

import java.util.Arrays;

public class Klientas implements Mokejimas {
    // cia yra elementai
    private String bankoSaskaita;
    private String asmensVardas;
    private Double suma;
    private Double[] sumuMasyvas;

    // konstruktoriaus kurimas
    public Klientas (String bankoSaskaita, String asmensVardas, Double suma){
        this.bankoSaskaita = bankoSaskaita;
        this.asmensVardas = asmensVardas;
        this.suma = suma;
    }

    public Klientas (String bankoSaskaita, String asmensVardas, Double suma, Double[] sumuMasyvas){
        // this kreipias i tos pacios klases konstruktoriu,taciau jo nesugadina, turedamas tuos pacius duomenys kontruktoriuje
        this(bankoSaskaita, asmensVardas,  didziausiaSuma(sumuMasyvas,suma));
        this.sumuMasyvas = sumuMasyvas;

    }

    public static Double didziausiaSuma(Double[] sumuMasyvas, Double suma){
        // nereikia sito jeigu perduodi virsuje
//        Double suma = 0d;
        for (int i = 0; i < sumuMasyvas.length; i++){
            suma += sumuMasyvas[i];
        }
        return suma;
    }

    public String toString(){
        return "Banko saskaita: " + gautiBankoSaskaita() + " Asmens vardas: " + gautiSaskaitosAsmensVarda() + " Suma: " + gautiSuma()
                + " Saskaitos sumos: " + Arrays.toString(this.sumuMasyvas) + "\n";
    }


    // cia yra metodai
    @Override
    public String gautiBankoSaskaita() {
        return bankoSaskaita;
    }

    @Override
    public String gautiSaskaitosAsmensVarda() {
        return asmensVardas;
    }

    @Override
    public Double gautiSuma() {
        return suma;
    }

    @Override
    public Double[] gautiSumas() {
        return sumuMasyvas;
    }

}
